import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Card, Container } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import ArrowIcon from '@material-ui/icons/ExpandMoreRounded';
import theme from '../src/theme';

const useDatesStyles = makeStyles(theme => ({
  dateCard: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: 8,
    width: '12%',
    overflow: 'visible',
    borderRadius: 'unset',
    transition: '0.2s linear',
  },
  arrow: {
    bottom: -27,
    left: 0,
    right: 0,
    margin: 'auto',
    position: 'absolute',
    color: theme.palette.primary.accent,
  },
}));

function WeekView() {
  const [state, setState] = React.useState({ activeDayIndex: 0 });
  
  const setActiveDay = activeDayIndex => setState({ activeDayIndex });
  
  const dayTitles = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'];
  const today = new Date();
  const daysToRender = [
    {
      title: dayTitles[today.getDay()],
      weekDay: today.getDay(),
      day: (today.getDate()).toString().length === 1 ? `0${today.getDate()}` : today.getDate(),
      month: (today.getMonth() + 1).toString().length === 1 ? `0${today.getMonth() + 1}` : today.getMonth() + 1,
    },
  ];
  for(let i=1; i<=5; i++) {
    const day = new Date();
    day.setDate(today.getDate() + i);
    
    daysToRender.push({
      title: dayTitles[day.getDay()],
      weekDay: day.getDay(),
      day: (day.getDate()).toString().length === 1 ? `0${day.getDate()}` : day.getDate(),
      month: (today.getMonth() + 1).toString().length === 1 ? `0${today.getMonth() + 1}` : today.getMonth() + 1,
    })
  }
  
  const classes = useDatesStyles();
  
  return(
    <Container maxWidth='sm' style={{ display: 'flex', justifyContent: 'space-between' }}>
      {daysToRender.map((day, index) =>
        <Card
          {...(index === state.activeDayIndex && { raised: true })}
          key={`${day.day}${day.weekDay}`}
          style={{
            background: index === state.activeDayIndex
              ? theme.palette.primary.accent : theme.palette.background.default,
            color: index === state.activeDayIndex
              ? theme.palette.primary.contrastText : theme.palette.primary.dark,
            ...(index !== state.activeDayIndex && { boxShadow: 'none' }),
          }}
          className={classes.dateCard}
          onClick={() => setActiveDay(index)}
        >
          <div style={{ display: 'flex', flexDirection: 'column', alignItems:'space-between' }}>
            <Typography variant='h6'>{day.day}</Typography>
          </div>
          <Typography variant='caption'>{day.title}</Typography>
          {index === state.activeDayIndex && <ArrowIcon className={classes.arrow}/>}
        </Card>
      )}
    </Container>
  );
}

export default WeekView;
