import React from 'react'
import Modal from '@material-ui/core/Modal';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles(theme => ({
  paper: {
    position: 'absolute',
    width: '90vw',
    height: 'fit-content',
    maxHeight: '85vh',
    overflowY: 'scroll',
    padding: 15,
    backgroundColor: theme.palette.background.paper,
    boxShadow: `0px 0px 15px ${theme.palette.primary.dark}`,
    left: 0, right: 0, bottom: 0, top: 0,
    margin: 'auto',
  },
  close: {
    position: 'absolute',
    top: 7,
    right: 7,
  },
}));

export default function ({ open, onClose, children }) {
  const classes = useStyles();
  
  return (
    <Modal {...{ open, onClose }}>
      <div className={classes.paper}>
        <CloseIcon className={classes.close} onClick={onClose}/>
        {children}
      </div>
    </Modal>
  )
}
