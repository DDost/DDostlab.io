import React from 'react';
import Link from 'next/link';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Button } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  page: {
    width: '100vw',
    height: '100vh',
    backgroundImage: 'url("static/img/bg-login.jpg")',
    backgroundSize: '168%',
    backgroundRepeat: 'no-repeat',
    backgroundPositionY: 360,
    backgroundPositionX: 55,
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: theme.palette.primary.dark,
    
  },
  overlay: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(255,255,255,0.25)',
    position: 'absolute',
    zIndex: 1,
    overflow: 'hidden',
  },
  content: {
    width: '80%',
    height: '90%',
    position: 'relative',
    zIndex: 2,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 25,
  },
  text: {
    textAlign: 'center',
    padding: 15,
    width: '100%',
    margin: '15px 0px 70px 0px',
  },
  title: {
    fontFamily: 'Mano',
    fontSize: 44,
    fontWeight: 'normal',
    marginBottom: 50,
  },
  buttons: {
  
  },
  button: {
    marginBottom: 10,
    width: '100%',
    backgroundColor: theme.palette.primary.dark,
  },
  registerBtn : {
    width: 'fit-content !important',
    padding: '0px 10px',
    fontWeight: 'bold',
    color: theme.palette.primary.contrastText,
    borderRadius: 'unset',
    borderBottom: `1px solid ${theme.palette.primary.contrastText}`,
  },
}));

function LoginPage(){
  const classes = useStyles();
  
  return(
    <section className={classes.page}>
      <div className={classes.overlay}/>
      <div className={classes.content}>
        <div style={{ display: 'flex', alignItems: 'center', flexDirection: 'column', marginTop: '40%' }}>
          <h2 className={classes.title}>Hair Craft</h2>
          <Typography variant='caption' className={classes.text}>
            Стилист - это постоянное движение вперёд, это инновации и детали, которые важно не упускать.
            Важно, чтобы они всегда были рядом с ним.
          </Typography>
          <div className={classes.buttons}>
            <Link href='/'>
              <Button className={classes.button} variant='contained' color='primary'>Войти по номеру телефона</Button>
            </Link>
            <Link href='/'>
              <Button className={classes.button} variant='contained' color='primary'>Войти через вк</Button>
            </Link>
          </div>
        </div>
        <Button className={classes.registerBtn} style={{ width: '100%' }} color='primary'>Регистрация</Button>
      </div>
    </section>
  )
}

export default LoginPage;
