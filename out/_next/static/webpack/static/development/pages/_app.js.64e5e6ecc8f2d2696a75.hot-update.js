webpackHotUpdate("static/development/pages/_app.js",{

/***/ "./src/theme.js":
/*!**********************!*\
  !*** ./src/theme.js ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _material_ui_core_styles_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-ui/core/styles/index */ "./node_modules/@material-ui/core/styles/index.js");
/* harmony import */ var _material_ui_core_styles_index__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles_index__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_colors_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/colors/index */ "./node_modules/@material-ui/core/colors/index.js");
/* harmony import */ var _material_ui_core_colors_index__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_colors_index__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _static_fonts_ProximaNova_Light_ttf__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../static/fonts/ProximaNova-Light.ttf */ "./static/fonts/ProximaNova-Light.ttf");
/* harmony import */ var _static_fonts_ProximaNova_Light_ttf__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_static_fonts_ProximaNova_Light_ttf__WEBPACK_IMPORTED_MODULE_2__);



var proxima = {
  fontFamily: 'Jura sans-serif',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 400,
  src: "\n    local('Proxima'),\n    local('Proxima-Light'),\n    url(".concat(_static_fonts_ProximaNova_Light_ttf__WEBPACK_IMPORTED_MODULE_2___default.a, ") format('ttf')\n  ")
}; // Create a theme instance.

var theme = Object(_material_ui_core_styles_index__WEBPACK_IMPORTED_MODULE_0__["createMuiTheme"])({
  palette: {
    primary: {
      light: '#ccc',
      main: '#525252',
      dark: '#000000',
      contrastText: '#ffffff'
    },
    secondary: {
      light: '#ffff6b',
      main: '#fff',
      dark: '#c6a700',
      contrastText: '#000000',
      positive: '#23ba46'
    },
    error: {
      main: _material_ui_core_colors_index__WEBPACK_IMPORTED_MODULE_1__["red"].A400
    },
    background: {
      "default": '#fff'
    }
  },
  typography: {
    fontFamily: 'Proxima'
  },
  overrides: {
    MuiButton: {
      contained: {
        borderRadius: 0
      }
    },
    MuiCssBaseline: {
      '@global': {
        '@font-face': [proxima]
      }
    }
  }
});
/* harmony default export */ __webpack_exports__["default"] = (theme);

/***/ }),

/***/ "./static/fonts/ProximaNova-Light.ttf":
/*!********************************************!*\
  !*** ./static/fonts/ProximaNova-Light.ttf ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/fonts/ProximaNova-Light-0188899cd3ec92cdcaa4c97ca0c75247.ttf";

/***/ })

})
//# sourceMappingURL=_app.js.64e5e6ecc8f2d2696a75.hot-update.js.map