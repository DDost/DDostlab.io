import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Avatar from '@material-ui/core/Avatar';
import Link from 'next/link';
import Layout from '../components/Layout';
import QuickLinks from '../components/QuickLinks';


const useStyles = makeStyles(theme => ({
  top: {
    display: 'flex',
    margin: '20px 0px 0px 0px',
    justifyContent: 'space-between',
  },
  bottom: {
    marginBottom: '20px',
    position: 'relative',
    
    '&:after': {
      content: '',
      position: 'absolute',
      left: '-80%', bottom: '5%',
      transform: 'rotate(25deg)',
      width: 1000,
      height: 500,
      backgroundColor: '#000',
      zIndex: -1,
    },
  },
  headerInfo: {
    display: 'flex',
    alignItems: 'flex-start',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  exit: {
    width: 18,
    height: 18,
    fill: theme.palette.primary.main,
    marginLeft: 5,
  },
  briefing: { display: 'flex', justifyContent: 'space-between' },
  avatar: {
    width: '100px',
    height: '100px',
    
    '& img' : { objectPosition: 'top' },
  },
  date: { textTransform: 'capitalize' },
  logout: { marginLeft: 15 },
}));

const date = new Date().toLocaleDateString('ru', { month: 'long', day: 'numeric', weekday: 'short' })

function MainPage() {
  const classes = useStyles();
  return (
    <Layout menu={false}>
        <Container className={classes.top}>
          <div className={classes.headerInfo}>
            <Typography variant='h5'><b>Ирина</b></Typography>
            <Typography variant='h6' className={classes.date}>{date}</Typography>
          </div>
          <Avatar src='static/img/avatar2.jpg' className={classes.avatar}/>
        </Container>
      <Container className={classes.bottom}>
        {/*<div className={classes.briefing}>*/}
        {/*  <Typography variant='body1'>Записей на сегодня: <b>3</b></Typography>*/}
        {/*  <Typography variant='body1'>Ближайшая: <b>13:00</b></Typography>*/}
        {/*</div>*/}
      <QuickLinks style={{ marginTop: 50 }}/>
      </Container>
    </Layout>
  );
}

export default MainPage;
