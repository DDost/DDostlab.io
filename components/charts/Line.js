import React, { useEffect, useState } from 'react';
import { makeStyles, MenuItem, Select, Typography } from '@material-ui/core';
import { Line, LineChart, Tooltip, XAxis } from 'recharts';

const useStyles = makeStyles(() => ({
  chartHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
  },
}));

function LineStats({ data, title, plusLabel }) {
  const [chartData, setData] = useState([]);
  const [period, setPeriod] = useState(6);
  const classes = useStyles();
  
  useEffect(() => {
    setData(data.slice(-period))
  }, [period]);
  
  return (
    <React.Fragment>
      <div className={classes.chartHeader}>
        <Typography variant='subtitle1'><b>{title}:</b></Typography>
        <Select value={period} onChange={e => setPeriod(e.target.value)}>
          <MenuItem value={3}>3 мес</MenuItem>
          <MenuItem value={6}>6 мес</MenuItem>
          <MenuItem value={12}>12 мес</MenuItem>
        </Select>
      </div>
      <LineChart
        mirror
        width={350}
        height={200}
        data={chartData}
        margin={{ top: 30, right: 20, left: 20, bottom: 5 }}
      >
        <XAxis dataKey='name'/>
        <Tooltip formatter={value => [plusLabel ? `+ ${value}` : value, null ]}/>
        <Line
          label={({ value, x, y, stroke }) =>
            <text
              {...{ x, y }}
              {...(value > 0 ? { dx: -10 } : { dx: -3 })}
              dy={-10}
              fill={stroke}
            >
              {value > 0 && plusLabel && '+'}{value}
            </text>
          }
          type='monotone'
          strokeWidth={2}
          dataKey='value'
          stroke='#000'
          activeDot={{ r: 8 }}
        />
      </LineChart>
    </React.Fragment>
  )
}

export default LineStats;
