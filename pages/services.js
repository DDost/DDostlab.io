import React from 'react';
import {
  Tabs,
  Tab,
  Container,
  TextField,
  InputAdornment,
  Card,
  Typography,
  Button,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import makeStyles from '@material-ui/styles/makeStyles';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Layout from '../components/Layout';
import Modal from '../components/toolbox/Modal';

const useStyles = makeStyles(() => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: 10,
  },
  button: { marginTop: 30 },
  cardsWrapper: {
    display: 'flex',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    marginTop: 20,
  },
  card: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '47%',
    height: 150,
    marginBottom: 20,
    borderRadius: 'unset',
  },
  cardImg: {
    objectFit: 'contain',
    width: '100%',
    height: '80%',
    marginBottom: 5,
  },
  modal: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    '& img': {
      width: '100%',
    },
  },
  modalItem: {
    marginTop: 20,
    width: '100%',
  },
  modalCaption: {
    padding: 10,
    display: 'block',
  },
  modalBtns: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginBottom: 10,
    marginRight: 10
  },
}));


const hairCuts = [
  {
    id: '1',
    img: 'static/img/login-bg.png',
    imgDesc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet asperiores at, aut corporis dolor dolore ducimus eveniet fuga fugiat ipsam minus neque officiis optio, pariatur quo quod rem rerum saepe veritatis voluptates. A, qui tenetur.',
    schema: 'https://via.placeholder.com/728x350',
    schemaDesc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus at commodi dolorem laborum laudantium magnam, maxime natus neque numquam omnis pariatur, possimus reiciendis soluta vel.',
    video: 'https://www.youtube.com/embed/VfDFUDyCv48',
    videoDesc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis cumque, itaque nam quo voluptate voluptates?',
    title: 'Стрижка 1',
  },
  {
    id: '2',
    img: 'static/img/login-bg.png',
    imgDesc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet asperiores at, aut corporis dolor dolore ducimus eveniet fuga fugiat ipsam minus neque officiis optio, pariatur quo quod rem rerum saepe veritatis voluptates. A, qui tenetur.',
    title: 'Стрижка 2',
  },
  {
    id: '3',
    img: 'static/img/login-bg.png',
    imgDesc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet asperiores at, aut corporis dolor dolore ducimus eveniet fuga fugiat ipsam minus neque officiis optio, pariatur quo quod rem rerum saepe veritatis voluptates. A, qui tenetur.',
    title: 'Стрижка 3',
  },
  {
    id: '4',
    img: 'static/img/login-bg.png',
    imgDesc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet asperiores at, aut corporis dolor dolore ducimus eveniet fuga fugiat ipsam minus neque officiis optio, pariatur quo quod rem rerum saepe veritatis voluptates. A, qui tenetur.',
    title: 'Стрижка 4',
  },
  {
    id: '5',
    img: 'static/img/login-bg.png',
    imgDesc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet asperiores at, aut corporis dolor dolore ducimus eveniet fuga fugiat ipsam minus neque officiis optio, pariatur quo quod rem rerum saepe veritatis voluptates. A, qui tenetur.',
    title: 'Стрижка 5',
  },
];


function Services() {
  const [activeTab, setActiveTab] = React.useState(0);
  const [modalContent, setModalContent] = React.useState(null);
  const classes = useStyles();
  
  const toggleTabs = tab => setActiveTab(tab);
  const toggleModal = content => setModalContent(content);
  
  return(
    <Layout>
       <Container className={classes.container}>
         <TextField
           placeholder='Поиск'
           InputProps={{
             startAdornment: (
               <InputAdornment position='start'>
                 <SearchIcon/>
               </InputAdornment>
             ),
           }}
         />
         <Tabs
           value={activeTab}
           onChange={(e, tab) => toggleTabs(tab)}
           variant='fullWidth'
           style={{ marginTop: 10 }}
         >
           <Tab value={0} label='Стрижки'/>
           <Tab value={1} label='Окрашивания'/>
           <Tab value={2} label='Стайлинг'/>
         </Tabs>
         <Button className={classes.button} variant='contained'>
           Создать
           <AddIcon/>
         </Button>
         <div className={classes.cardsWrapper}>
           {hairCuts.map(el =>
             <Card key={el.id} className={classes.card} onClick={() => toggleModal(el)}>
               <img className={classes.cardImg} src={el.img} alt='service'/>
               <Typography variant='subtitle2'>{el.title}</Typography>
             </Card>
           )}
         </div>
         <Modal
           aria-labelledby='simple-modal-title'
           aria-describedby='simple-modal-description'
           open={modalContent !== null}
           onClose={() => toggleModal(null)}>
           {modalContent !== null &&
            <div className={classes.modal}>
              <Typography variant='h6'>{modalContent.title}</Typography>
              {modalContent.img &&
              <Card className={classes.modalItem}>
                <img src={modalContent.img} alt={modalContent.title}/>
                <Typography className={classes.modalCaption} variant='caption'>{modalContent.imgDesc}</Typography>
                <div className={classes.modalBtns}>
                  <EditIcon fontSize='small' style={{ marginRight: 10 }}/>
                  <DeleteIcon fontSize='small'/>
                </div>
              </Card>
              }
              {modalContent.schema &&
              <Card className={classes.modalItem}>
                <img src={modalContent.schema} alt={modalContent.title}/>
                <Typography className={classes.modalCaption} variant='caption'>{modalContent.schemaDesc}</Typography>
                <div className={classes.modalBtns}>
                  <EditIcon fontSize='small' style={{ marginRight: 10 }}/>
                  <DeleteIcon fontSize='small'/>
                </div>
              </Card>
              }
              {modalContent.video &&
              <Card className={classes.modalItem}>
                <iframe
                  title={modalContent.title}
                  width='100%'
                  height='315'
                  src={modalContent.video}
                  frameBorder='0'
                  allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
                  allowFullScreen
                />
                <Typography className={classes.modalCaption} variant='caption'>{modalContent.videoDesc}</Typography>
                <div className={classes.modalBtns}>
                  <EditIcon fontSize='small' style={{ marginRight: 10 }}/>
                  <DeleteIcon fontSize='small'/>
                </div>
              </Card>
              }
              <Button
                className={classes.button}
                variant='contained'
                style={{ width: '100%' }}
              >
                Добавить описание
                <AddIcon/>
              </Button>
            </div>
           }
         </Modal>
       </Container>
    </Layout>
  )
}

export default Services;
