import React from 'react';
import { get } from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { InputAdornment, TextField, Container, Card, Typography, Button } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import AddIcon from '@material-ui/icons/Add';
import FileIcon from '@material-ui/icons/FileCopy';
import { server } from '../env-config';
import Layout from '../components/Layout';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  button: {
    marginTop: 20,
  },
  feed: {
    marginTop: 20,
    display: 'flex',
    justifyContent: 'space-between',
  },
  feedColumn: {
    display: 'flex',
    flexDirection: 'column',
    width: '47%',
  },
}));


function MediaItem({ id, src, type, name, description }) {
  return (
    <Card key={id} style={{ width: '100%', height: 'fit-content', marginBottom: 30, borderRadius: 'unset' }}>
      {type === 'video' && <video controls autoPlay loop='loop' src={src} style={{ width: '100%', height: 'auto' }}/>}
      {type === 'img' && <img src={src} style={{ objectFit: 'contain', width: '100%' }}/>}
      {type === 'file' &&
      <a download href={src} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', margin: '10px 0px', textDecoration: 'none' }}>
        <FileIcon color='primary'/>
        <span style={{ marginLeft: 10, color: '#000' }}>{name}</span>
      </a>}
      <Typography style={{ margin: 5 }} variant='subtitle2'>{description}</Typography>
    </Card>
  )
}

function MediaPage({ media }) {
  const classes = useStyles();
  
  return(
    <Layout>
      <Container className={classes.container}>
        <TextField
          placeholder='Поиск'
          InputProps={{
            startAdornment: (
              <InputAdornment position='start'>
                <SearchIcon/>
              </InputAdornment>
            ),
          }}
        />
        <Button className={classes.button} variant='contained'>
          Создать
          <AddIcon/>
        </Button>
        {media
          ? media.length > 0 &&
          <div className={classes.feed}>
            <div className={classes.feedColumn}>
              {media.map((item, index) => index % 2 === 0 && <MediaItem key={item.id} {...item}/>)}
            </div>
            <div className={classes.feedColumn}>
              {media.map((item, index) => index % 2 !== 0 && <MediaItem key={item.id} {...item}/>)}
            </div>
          </div>
          : <Typography variant='subtitle1'>Загрузка...</Typography>}
      </Container>
    </Layout>
  )
}

MediaPage.getInitialProps = async () => {
  const media = await get(`${server}/static/media.json`).then(result => result.data);
  return { media };
};

export default MediaPage;
