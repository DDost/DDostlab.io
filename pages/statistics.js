import React, { useState } from 'react';
import { get } from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Container, Tab, Tabs, Card, Avatar, Select, MenuItem } from '@material-ui/core';
import Layout from '../components/Layout';
import LineChart from '../components/charts/Line';
import PieChart from '../components/charts/Pie';
import BarChart from '../components/charts/Bar';
import { server } from '../env-config';
import { ActiveClients, Attendance, IncomesAndExpenses } from '../components/clientsStats';

const useStyles = makeStyles(() => ({
  container: {},
  header: { marginBottom: 10 },
  card: { padding: 15, margin: 10 },
  avatar: { marginRight: 10 },
  option: { display: 'flex', alignItems: 'center' },
}));

function StatsPage({ statistics, clients }) {
  
  const classes = useStyles();
  const {
    clients_new: newClients,
    clients_overall: clientsOverall,
    clients_attendance: attendance,
    services_overall: { men: servicesOverallMen, women: servicesOverallWomen },
    finances,
    appointments,
    appointments_days: appointmentsDays,
  } = statistics;
  const [activeTab, toggleTab] = useState(0);
  const gendersData = [{ name: 'Мужчины', value: clientsOverall.men }, { name: 'Женщины', value: clientsOverall.women }]
  const daysLoadData = appointmentsDays.map(day => ({ name: day.day, value: day.value }));
  
  return (
    <Layout>
      <Container style={{ marginTop: 50 }} className={classes.container}>
        <Typography variant='h5' className={classes.header}>Статистика</Typography>
        <Tabs
          value={activeTab}
          variant='fullWidth'
          onChange={(e, tab) => toggleTab(tab)}
        >
          <Tab value={0} label='Клиенты'/>
          <Tab value={1} label='Записи'/>
          <Tab value={2} label='Сервисы'/>
          <Tab value={3} label='Финансы'/>
        </Tabs>
        {activeTab === 0 &&
        <section style={{ marginTop: 20}}>
          <PieChart
            title={<span>Всего клиентов в базе: <b>{clientsOverall.total}</b></span>}
            data={gendersData}
            showTotalLegend={false}
          />
          <div style={{ marginTop: 40 }}><LineChart plusLabel title='Новые клиенты' data={newClients}/></div>
          <div style={{ marginTop: 40 }}><ActiveClients {...{ clientsOverall }}/></div>
          <div style={{ marginTop: 40 }}><Attendance {...{ clients, attendance }}/></div>
        </section>
        }
        {activeTab === 1 &&
        <section style={{ marginTop: 40 }}>
          {appointments.length > 0 && <LineChart title='Записи, динамика' data={appointments}/>}
          {appointmentsDays.length > 0 && <div style={{ marginTop: 20 }}>
            <BarChart title='Записей в день (в среднем)' data={daysLoadData}/>
          </div>}
        </section>}
        {activeTab === 2 &&
        <section style={{ marginTop: 20 }}>
          {servicesOverallMen &&
          <PieChart
            title={<b>Мужчины</b>}
            data={servicesOverallMen}
            showTotalLegend={false}
            legendAlign='left'
            legendLayout='horizontal'
            legendVerticalAlign='bottom'
            colors={['#000', '#444', '#ccc', '#888', '#222', '#aaa']}
            height={380}
          />}
          {servicesOverallWomen &&
          <div style={{ marginTop: 50 }}>
            <PieChart
              title={<b>Женщины</b>}
              data={servicesOverallWomen}
              showTotalLegend={false}
              legendAlign='left'
              legendLayout='horizontal'
              legendVerticalAlign='bottom'
              colors={['#000', '#444', '#ccc', '#888', '#222', '#aaa']}
              height={380}
            />
          </div>}
        </section>
        }
        {activeTab === 3 &&
        <section style={{ marginTop: 20 }}>
          <IncomesAndExpenses data={finances}/>
        </section>
        }
      </Container>
    </Layout>
  );
}

StatsPage.getInitialProps = async () => {
  const statistics = await get(`${server}/static/statistics.json`).then(result => result.data);
  const clients = await get(`${server}/static/clients.json`).then(result => result.data);
  return { statistics, clients };
};

export default StatsPage;
