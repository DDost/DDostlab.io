import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Link from 'next/link';
import {
  Box,
  AppBar,
  Toolbar,
  IconButton,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import SideMenu from './SideMenu';

const useStyles = makeStyles(theme => ({
  root: { flexGrow: 1, overflowX: 'hidden' },
  menuButton: { marginRight: theme.spacing(2), position: 'fixed', left: 15 },
  logout: { position: 'fixed', left: 15 },
  appBar: { backgroundColor: theme.palette.primary.dark },
  toolBar: { justifyContent: 'center' },
  title: { fontFamily: 'Mano', fontSize: 32, fontWeight: 'normal', margin: 0 },
}));

function Layout({ children, menu = true }) {
  const classes = useStyles();
  const [state, setState] = React.useState({ menuIsOpen: false });
  
  const toggleMenu = () => setState({ menuIsOpen: !state.menuIsOpen });
  
  return (
    <div className={classes.root}>
      <AppBar position='fixed' className={classes.appBar}>
        <Toolbar className={classes.toolBar}>
          {menu && <IconButton
            className={classes.menuButton}
            edge='start'
            color='inherit'
            aria-label='menu'
            onClick={toggleMenu}
          >
            <MenuIcon/>
          </IconButton>}
          {!menu &&
          <Link href='/login'>
            <img className={classes.logout} src='../static/icons/logout.svg' alt='log out' width={20} height={20}/>
          </Link>}
          <Link href='/'>
            <h2 className={classes.title}>Hair Craft</h2>
          </Link>
        </Toolbar>
      </AppBar>
      <Box my={10}>
        {children}
      </Box>
      {menu && <SideMenu open={state.menuIsOpen} onClose={toggleMenu}/>}
    </div>
  );
}

export default Layout;
