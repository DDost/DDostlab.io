import React, { useState, useEffect } from 'react';
import { get } from 'axios';
import Calendar from 'react-calendar/dist/entry.nostyle';
import { makeStyles, Container, Typography, Divider, Button } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import { server } from '../env-config';
import Layout from '../components/Layout';
import AppointmentsList from '../components/AppointmentsList';

const useStyles = makeStyles(theme => ({
  monthName: {
    color: theme.palette.primary.accent,
    letterSpacing: 2,
  },
  calendar: {
    marginTop: 20,
    '& .react-calendar__month-view__weekdays': { textAlign: 'center', marginBottom: 10 },
    '& .react-calendar__month-view__days__day': { height: 40, fontSize: 16 },
    '& .react-calendar__month-view__days__day--neighboringMonth': { color: theme.palette.primary.light },
    '& .react-calendar__tile--now': { backgroundColor: `${theme.palette.primary.dark} !important`, border: 'unset', color: theme.palette.primary.contrastText },
    '& .react-calendar__tile--active': { border: '2px solid #000', backgroundColor: 'unset' },
    '& .react-calendar__tile--now, & .react-calendar__tile--active': {
      transition: '.3s linear',
      outline: 'none',
    },
  },
  events: {
    marginTop: 20,
    display: 'flex',
    flexDirection: 'column',
  },
  eventsHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'baseline',
    color: theme.palette.primary.accent,
  },
  button: { marginLeft: 'auto' },
  notes: {
    display: 'flex',
    flexDirection: 'column',
  },
}));

/**
 * @return {string}
 */
function CalendarPage({ appointments }) {
  const [date, setDate] = useState();
  const classes = useStyles();
  useEffect(() => setDate(new Date()), []);
  
  if (!date) return <div>Загрузка...</div>;
  
  const renderDayTitle = () => {
    const today = new Date();
    const tomorrow = new Date();
    const yesterday = new Date();
    
    const localDate = date.toLocaleDateString().split('/').join(' / ').slice(0, -7);
    
    tomorrow.setDate(today.getDate() + 1);
    yesterday.setDate(today.getDate() - 1);
  
    if (date.getDate() === today.getDate()) return (
      <React.Fragment>
        <Typography variant='h6'><b>Сегодня</b></Typography>
        <Typography variant='body1'>{localDate}</Typography>
      </React.Fragment>
    );
    if (date.getDate() === tomorrow.getDate()) return (
      <React.Fragment>
        <Typography variant='h6'><b>Завтра</b></Typography>
        <Typography variant='body1'>{localDate}</Typography>
      </React.Fragment>
    );
    if (date.getDate() === yesterday.getDate()) return (
      <React.Fragment>
        <Typography variant='h6'><b>Вчера</b></Typography>
        <Typography variant='body1'>{localDate}</Typography>
      </React.Fragment>
    );
    return <Typography variant='h6'><b>{localDate}</b></Typography>
  };
  
  return(
    <Layout>
      <Container>
        <Typography className={classes.monthName} variant='h4'><b>{date.toLocaleString('ru', { month: 'long' })}</b></Typography>
        <Typography variant='subtitle2'>{date.getFullYear()}</Typography>
        <Calendar
          locale='ru'
          value={date}
          onChange={setDate}
          showNavigation={false}
          className={classes.calendar}
        />
        <div className={classes.events}>
          <div className={classes.eventsHeader}>{renderDayTitle()}</div>
          <Divider/>
          <AppointmentsList/>
          <div className={classes.notes}>
            <h2>Заметки:</h2>
            <Typography varinat='body1' color='primary'>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Aliquam delectus dolore doloribus esse fugiat harum impedit,
              ipsum numquam placeat quisquam rem, voluptas, voluptatibus!
              Cumque ipsum mollitia vitae. Inventore, maxime sunt!
            </Typography>
            <Button className={classes.button} variant='contained'><EditIcon/></Button>
          </div>
        </div>
      </Container>
    </Layout>
  )
}

CalendarPage.getInitialProps = async () => {
  const {  data: appointments } = await get(`${server}/static/appointments.json`);
  return { appointments }
};

export default CalendarPage;
