import React from 'react';
import { Cell, Legend, Pie, PieChart, Tooltip } from 'recharts';
import { makeStyles, Typography } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  chartWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    margin: 10,
  },
  title: { width: '100%', textAlign: 'left' },
  label: {  },
}));

const radian = Math.PI / 180;

const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent }) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * radian);
  
  const y = cy + radius * Math.sin(-midAngle * radian);
  
  if (percent !== 0) {
    return (
      <text x={x} y={y} fill='white' textAnchor={x > cx ? 'start' : 'end'} dominantBaseline='central'>
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  }

};

function PieStats({
  data,
  title,
  units = '',
  height = 250,
  showTotalLegend = true,
  colors = ['#000', '#666', '#aaa'],
  legendLayout = 'vertical',
  legendAlign = 'right',
  legendVerticalAlign = 'middle',
}) {
  const classes = useStyles();
  
  return (
    <section className={classes.chartWrapper}>
      {title && <Typography variant='body1' className={classes.title}>{title}</Typography>}
      <PieChart {...{ height }} width={360}>
        <Pie {...{ data }} dataKey='value' labelLine={false} label={renderCustomizedLabel}>
          {data.map((entry, index) => (
            // eslint-disable-next-line react/no-array-index-key
            <Cell key={`${entry.value}-${index}`} fill={colors[index % colors.length]}/>
          ))}
        </Pie>
        <Tooltip formatter={el => `${el} ${units}`}/>
        <Legend
          align={legendAlign}
          verticalAlign={legendVerticalAlign}
          layout={legendLayout}
          payload={[
            ...data.map((el, i) => ({
              id: `${el.value}_${i}`,
              type: 'square',
              value: (
                <span>
                  {el.name}: <b className={classes.label}>{el.value}</b> {units}
                </span>
              ),
              color: colors[i],
            })),
            ...(showTotalLegend ? [
              {
                id: `total`,
                type: 'square',
                value: (
                  <span>
                    Всего: <b>{data.reduce((acc, el) => acc + el.value, 0)}</b> {units}
                  </span>
                ),
                color: 'transparent',
              },
            ] : []),
          ]}
        />
      </PieChart>
    </section>
  )
}

export default PieStats;
