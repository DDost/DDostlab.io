import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Divider, Typography, Avatar, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails } from '@material-ui/core';
import DurationIcon from '@material-ui/icons/TimelapseRounded';
import LocationIcon from '@material-ui/icons/LocationOn';
import CallIcon from '@material-ui/icons/Call';
import theme from '../src/theme';

const useAppointmentStyles = makeStyles(() => ({
  card: {
    width: '80%',
    padding: '0px 10px',
    marginBottom: 50,
    borderRadius: 'unset',
  },
  noPadding: { padding: 0 },
  info: { display: 'flex', alignItems: 'center' },
  social: {
    display:'flex',
    justifyContent: 'space-around',
    flexDirection: 'row',
    alignItems: 'center',
    margin: '10px 0px',
  },
  iconCall: {
    width: 25,
    height: 25,
    padding: 5,
    borderRadius: '50%',
    background: theme.palette.primary.dark,
  },
  iconSocial: {
    width: 30,
    height: 30,
    padding: 1,
    marginLeft: 10,
    borderRadius: 0,
  },
  time: {
    position: 'absolute',
    top: 0,
    left: -75,
    color: theme.palette.primary.dark,
  },
}));

function Appointment({ client, time, duration, location, jobs, contacts }) {
  const classes = useAppointmentStyles();
  
  return(
    <ExpansionPanel className={classes.card}>
      <ExpansionPanelSummary className={classes.noPadding}>
        <div className={classes.info}>
          <Avatar src={client.photo}/>
          <Typography style={{ marginLeft: 15 }} variant='h6'>{client.name}</Typography>
        </div>
        <Typography variant='h6' className={classes.time}>{time}</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails className={classes.noPadding}>
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <Divider/>
          <div style={{ display: 'flex', justifyContent: 'space-between', marginTop: 10 }}>
            <div className={classes.info}>
              <LocationIcon color='action' style={{ width: 20 }}/>
              <Typography variant='subtitle2'>{location}</Typography>
            </div>
            <div className={classes.info}>
              <DurationIcon color='action' style={{ width: 20, marginRight: 3, marginLeft: 5 }}/>
              <Typography variant='subtitle2'>{duration}</Typography>
            </div>
          </div>
          <div style={{ margin: '20px 0px' }}>
            {jobs.map(job =>
              <div className='info' key={job.title}>
                <Typography
                  style={{ display: 'flex', justifyContent: 'space-between' }}
                  variant='subtitle1'>
                  <span style={{ marginRight: 10 }}>- {job.title}</span>
                  <b style={{ whiteSpace: 'pre' }}>{job.price} ₽</b>
                </Typography>
              </div>
            )}
            {jobs.length > 1 &&
            <Typography
              varinat='h6'
              style={{ width: '100%', textAlign: 'right', marginTop: 20 }}
            >
              <Divider/>
              <b>{jobs.reduce((a, b) => (a + b.price), 0)} ₽</b>
            </Typography>
            }
          </div>
          <div className={classes.social}>
            {contacts.phone && <a className={classes.info} style={{ height: 30 }} href={`tel:${contacts.phone}`}><CallIcon className={classes.iconCall} color='secondary'/></a>}
            {contacts.whatsapp && <a href={`https://wa.me/${contacts.whatsapp}`}><Avatar className={classes.iconSocial} src='../static/icons/whatsapp.svg'/></a>}
            {contacts.facebook && <Avatar className={classes.iconSocial} src='../static/icons/facebook.svg'/>}
            {contacts.telegram && <Avatar className={classes.iconSocial} src='../static/icons/telegram.svg'/>}
            {contacts.instagram && <Avatar className={classes.iconSocial} src='../static/icons/instagram.svg'/>}
            {contacts.vk && <Avatar className={classes.iconSocial} src='../static/icons/vk.svg'/>}
          </div>
        </div>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  )
}

export default Appointment;
