import React from 'react';
import {
  Drawer,
  List,
  ListItem,
  Typography,
  Divider,
  InputAdornment,
  TextField,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ClientsIcon from '@material-ui/icons/People';
import StatisticsIcon from '@material-ui/icons/BarChart';
import CalendarIcon from '@material-ui/icons/DateRange';
import SettingsIcon from '@material-ui/icons/Settings';
import MediaIcon from '@material-ui/icons/PermMedia';
import SearchIcon from '@material-ui/icons/Search';
import EditIcon from '@material-ui/icons/Edit';
import Link from 'next/link';

const ServicesIcon = () => <img src='static/icons/scissor.svg' alt='' style={{ width: 25, height: 25 }}/>

const menuItems = [
  { text: 'Клиенты', icon: <ClientsIcon/>, link: '/clients' },
  { text: 'Календарь', icon: <CalendarIcon/>, link: '/calendar' },
  { text: 'Сервисы', icon: <ServicesIcon/>, link: '/services' },
  { text: 'Медиа', icon: <MediaIcon/>, link: '/media' },
  { text: 'Скетчи', icon: <EditIcon/>, link: '/sketches' },
  // { text: 'Обучение', icon: <StudyIcon/>, link: '/study' },
  { text: 'Статистика', icon: <StatisticsIcon/>, link: '/statistics' },
  { text: 'Настройки', icon: <SettingsIcon/>, link: '/settings' },
];

const useStyles = makeStyles(() => ({
  bar: { '& .MuiDrawer-paper': { minWidth: 200 } },
  list: { height: '100%', position: 'relative' },
  menuItem: {
    display: 'flex',
    alignItems: 'flex-end',
    margin: '8px 0px',
  },
  menuItemText: {
    marginLeft: 20,
  },
  search: {
    padding: '0px 16px',
    marginBottom: 100,
  },
}));

function SideMenu({ open, onClose }) {
  const classes = useStyles();
  
  return(
    <Drawer {...{ open, onClose }} anchor='left' className={classes.bar}>
      <List className={classes.list}>
        {menuItems.map((item, index) =>
          <React.Fragment key={item.link}>
            <Link href={item.link}>
              <ListItem key={item.link} className={classes.menuItem}>
                {item.icon}
                <Typography className={classes.menuItemText}>
                  {item.text}
                </Typography>
              </ListItem>
            </Link>
            {index !== menuItems.length -1 && <Divider/>}
          </React.Fragment>
        )}
      </List>
      <TextField
        className={classes.search}
        placeholder='Поиск'
        InputProps={{
          startAdornment: (
            <InputAdornment position='start'>
              <SearchIcon/>
            </InputAdornment>
          ),
        }}
      />
    </Drawer>
  )
}

export default SideMenu;
