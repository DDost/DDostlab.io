import React from 'react';
import { makeStyles, Typography } from '@material-ui/core';
import { BarChart, Tooltip, Legend, Bar, XAxis, ResponsiveContainer } from 'recharts';

const useStyles = makeStyles(() => ({
  chartHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
  },
}));

function BarStats({
  data,
  title,
  layout = 'horizontal',
  plusLabel = false,
  interval = 0,
  rotate = false,
  margin = { top: 20 },
}) {
  const classes = useStyles();
  
  return (
    <React.Fragment>
      <div className={classes.chartHeader}>
        {title && <Typography variant='subtitle1'><b>{title}:</b></Typography>}
      </div>
        <BarChart {...{ data, layout, margin }} width={350} height={280}>
          <Tooltip formatter={value => [plusLabel ? `+ ${value}` : value, null ]}/>
          <Bar
            dataKey='value'
            fill='#000'
            label={({ value, x, y, stroke }) =>
              <text
                {...{ x, y }}
                dx={10}
                dy={-10}
                fill={stroke}
              >
                {value > 0 && plusLabel && '+'}{value}
              </text>
            }
          />
          <XAxis
            {...{ interval }}
            {...rotate && { angle: -45, textAnchor: 'end' }}
            dataKey={layout === 'horizontal' ? 'name' : 'value'}/>
        </BarChart>
    </React.Fragment>
  )
}

export default BarStats;
