import { createMuiTheme } from '@material-ui/core/styles/index';
import { red } from '@material-ui/core/colors/index';
import Lato from '../static/fonts/Lato-Medium.ttf';

const proxima = {
  fontFamily: 'Proxima Nova sans-serif',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 400,
  src: `
    local('Lato'),
    url(${Lato}) format('ttf')
  `,
};

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#ccc',
      main: '#525252',
      dark: '#000000',
      contrastText: '#ffffff',
      accent: '#0b1656',
    },
    secondary: {
      light: '#ffff6b',
      main: '#fff',
      dark: '#c6a700',
      contrastText: '#000000',
      positive: '#23ba46',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#fff',
    },
  },
  typography: {
    fontFamily: 'Proxima',
  },
  fontFamily: 'Proxima',
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '@font-face': [proxima],
      },
    },
    MuiButton: {
      contained: {
        borderRadius: 0,
        backgroundColor: '#000',
        color: '#fff',
      },
    },
    MuiTabs: {
      indicator: {
        backgroundColor: '#0b1656',
      },
    },
    MuiTab: {
      root: {
        '&$selected': {
          color: '#0b1656',
          fontWeight: 'bold',
        },
      },
    },
    MuiInput: {
      root: {
        color: '#525252',
        '&$focused': {
          color: '#0b1656',
          fontWeight: 'bold',
        },
      },
      underline: {
        "&&&&:hover:after": {
          borderBottom: "2px solid #0b1656",
        },
      },
    },
  },
});

export default theme;
