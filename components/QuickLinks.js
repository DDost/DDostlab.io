import React from 'react';
import { Container, Typography, Card } from '@material-ui/core';
import makeStyles from '@material-ui/styles/makeStyles';
import Link from 'next/link';
import theme from '../src/theme';

const links = [
  { title: 'Клиенты', path: '/clients', icon: '../static/icons/group.svg', bg: '../static/img/session/clientBg3.jpeg' },
  { title: 'Сервисы', path: '/services', icon: '../static/icons/scissor.svg', bg: '../static/img/session/services-2.jpg' },
  { title: 'Календарь', path: '/calendar', bg: '../static/img/session/calendar.jpg' },
  { title: 'Настройки', path: '/settings', icon: '../static/icons/settings-gears.svg' },
  { title: 'Медиа', path: '/media', icon: '../static/icons/files-and-folders.svg', bg: '../static/img/media6.jpeg' },
  { title: 'Скетчи', path: '/sketches', bg: '../static/img/session/NG463-4.jpeg' },
  { title: 'Поддержка', path: '/help', icon: '../static/icons/help-operator.svg' },
  { title: 'Статистика', path: '/statistics', bg: '../static/img/session/NG1337.jpg' },
];

const useStyles = makeStyles(() => ({
  container: {
    display: 'grid',
    gridTemplateColumns: 'repeat(2, 1fr)',
    gridTemplateRows: 'repeat(10, 1fr)',
    gridColumnGap: 10,
    gridRowGap: 10,
    position: 'relative',
    margin: 0,
    padding: 0,
    
  },
  link0: { gridArea: '1/1/3/3',
    // filter: 'unset !important',
    backgroundPositionY: '15%',
  },
  link1: { gridArea: '3/1/6/2', backgroundPositionX: -25 },
  link2: { gridArea: '3/2/5/3', color: 'black !important', filter: 'saturate(80%) !important' },
  link3: { gridArea: '10/2/10/2', color: 'white !important', backgroundColor: '#222' },
  link4: { gridArea: '5/2/8/3', backgroundPositionX: '30%', backgroundSize: '112% !important' },
  link5: {
    gridArea: '6/1/8/2',
    backgroundSize: '120% !important',
    backgroundPositionX: 'right',
    backgroundPositionY: 'bottom',
    color: 'black !important',
    filter: 'saturate(80%) !important',
  },
  link6: { gridArea: '10/1/10/2', color: 'white !important', backgroundColor: '#222' },
  link7: { gridArea: '8/1/10/3', backgroundPositionY: '78%', color: 'black !important' },
  link: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    borderRadius: 'unset',
    boxShadow: '3px 3px 6px 2px rgba(0,0,0,0.2)',
    padding: 25,
    backgroundSize: 'cover',
    filter: 'saturate(50%)',
    textTransform: 'uppercase',
    letterSpacing: 2,
    fontSize: 20,
  },
  icon: { width: 50, height: 50 },
  title: { fontWeight: 'bold' },
}));

function QuickLinks (props) {
  const classes = useStyles();
  
  return (
    <Container className={classes.container} {...props}>
      {links.map((el, i) =>
        <React.Fragment key={el.path}>
          <Link href={el.path}>
            <Card
              className={`${classes.link} ${classes[`link${i}`]}`}
              style={{
                backgroundImage: el.bg ? `url(${el.bg})` : 'unset',
                color: el.bg ? theme.palette.primary.contrastText : theme.palette.primary.dark,
              }}
            >
              {/*<img src={el.icon} alt={el.title} className={classes.icon}/>*/}
              <Typography className={classes.title}>{el.title}</Typography>
            </Card>
          </Link>
        </React.Fragment>
      )}
    </Container>
  );
}

export default QuickLinks;
