import React from 'react';
import { get } from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import {
  Avatar,
  Typography,
  Card,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from '@material-ui/core';
import CallIcon from '@material-ui/icons/Call';
import CanceledIcon from '@material-ui/icons/CancelOutlined';
import Button from '@material-ui/core/Button';
import ExpandMoreIcon from '@material-ui/icons/ExpandMoreRounded';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import { server } from '../env-config';
import Layout from '../components/Layout';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  photoBg: {
    zIndex: -1,
    position: 'relative',
    height: 300,
    objectFit: 'cover',
  },
  info: {
    position: 'relative',
    zIndex: 1,
    backgroundColor: theme.palette.primary.contrastText,
    
    '&:before':{
      content: '""',
      position: 'absolute',
      zIndex: -2,
      width: '120%',
      height: 100,
      backgroundColor: theme.palette.primary.contrastText,
      transform: 'rotate(-10deg)',
      top: -40,
      left: -20,
      boxShadow: '0px -10px 10px rgba(0,0,0,0.5)',
    },
  },
  infoContainer: {
    margin: '30px 20px 10px 20px',
  },
  social: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 70,
    marginTop: 20,
  },
  iconCall: {
    width: 27,
    height: 27,
    padding: 5,
    margin: 10,
    borderRadius: '50%',
    background: theme.palette.primary.dark,
  },
  iconSocial: {
    width: 40,
    height: 40,
    padding: 5,
    margin: 10,
    objectFit: 'contain',
  },
  appointments: {
    display: 'flex',
    flexDirection: 'column',
  },
  appointment: {
    padding: 10,
    marginTop: 10,
    borderRadius: 'unset',
  },
  service: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  cancelledLabel: {
    display: 'flex',
    alignItems: 'center',
    width: 'fit-content',
    padding: '2px 0px 2px 5px',
    marginLeft: 'auto',
    color: theme.palette.error.main,
  },
  button: { marginBottom: 10 },
  avatarMini: {
    position: 'absolute',
    left: 20,
    top: -60,
    width: 70,
    height: 70,
    borderRadius: '50%',
  },
  editIcon: {
    position: 'absolute',
    top: -20,
    right: 30,
  },
  expansion: {
    boxShadow: 'unset',
    '&:before': { display: 'none' },
  },
  expansionSummary: {
    width: 'fit-content',
    minHeight: 10,
    height: 30,
    marginBottom: 10,
    marginLeft: 10,
    padding: 0,
    borderBottom: `1px solid ${theme.palette.primary.light}`,
  },
  expansionDetails: {
    paddingLeft: 10,
    paddingBottom: 2,
    marginBottom: 20,
  },
}));

function Appointment({ data }) {
  const classes = useStyles();
  
  return (
    <Card key={data.id} className={classes.appointment}>
      <div className={classes.service}>
        <Typography variant='subtitle1'><b>{data.date.slice(0, -3)}</b></Typography>
        {data.status === 'canceled' &&
        <div className={classes.cancelledLabel}>
          <Typography variant='body2' style={{ marginRight: 5 }}>отмена</Typography>
          <CanceledIcon color='error'/>
        </div>
        }
      </div>
      {data.services.map(service =>
        <div key={`${service.id}_${service.price}`}>
          <div className={classes.service}>
            <Typography variant='body1'>{service.name}</Typography>
            <Typography variant='body1'><b>{service.price} ₽</b></Typography>
          </div>
          {service.recipe &&
          <ExpansionPanel className={classes.expansion}>
            <ExpansionPanelSummary className={classes.expansionSummary} expandIcon={<ExpandMoreIcon/>}>
              <Typography variant='subtitle2'>Рецепт:</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.expansionDetails}>
              <Typography variant='body2' style={{ whiteSpace: 'pre' }}>{service.recipe}</Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>}
        </div>,
      )}
      {data.services.length > 1 &&
      <Typography
        varinat='body1'
        style={{ width: '100%', textAlign: 'right', marginTop: 5, borderTop: '1px solid #ccc' }}
      >
        <b>{data.services.reduce((a, b) => (a + parseInt(b.price)), 0)} ₽</b>
      </Typography>
      }
    </Card>
  );
}

function ClientProfile ({ client }) {
  const classes = useStyles();
  return (
    <Layout>
      <div className={classes.container} style={{ overflowX: 'hidden' }}>
        <img className={classes.photoBg} src={client.photo} alt='client'/>
        <section className={classes.info}>
          <Avatar src={client.photo} className={classes.avatarMini}/>
          <EditIcon className={classes.editIcon}/>
          <div className={classes.infoContainer}>
            <Typography variant='h4'>{client.name}</Typography>
            <Typography variant='caption'>{client.description}</Typography>
            <div className={classes.social}>
              {client.social.tel &&
              <a href={`tel:${client.social.tel}`}><CallIcon
                className={classes.iconCall} color='secondary'/></a>}
              {client.social.whatsapp && <a href={`https://wa.me/${client.social.whatsapp}`}><Avatar
                className={classes.iconSocial} src='../static/icons/whatsapp.svg'/></a>}
              {client.social.fb &&
              <Avatar className={classes.iconSocial} src='../static/icons/facebook.svg'/>}
              {client.social.telegram &&
              <Avatar className={classes.iconSocial} src='../static/icons/telegram.svg'/>}
              {client.social.instagram &&
              <Avatar className={classes.iconSocial} src='../static/icons/instagram.svg'/>}
              {client.social.vk &&
              <Avatar className={classes.iconSocial} src='../static/icons/vk.svg'/>}
            </div>
            <div className={classes.appointments}>
              <Typography variant='h6' style={{ marginBottom: 10 }}><b>Записи</b></Typography>
              <Button className={classes.button} variant='contained'>
                Создать
                <AddIcon/>
              </Button>
              {client.appointments.upcoming.length > 0
                ? <div className={classes.container}>
                  <Typography variant='subtitle1'>Предстоящие:</Typography>
                  {client.appointments.upcoming.map(el =>
                    <Appointment data={el} key={el.id}/>,
                  )}
                </div>
                : <div className={classes.appointments}>
                  <Typography variant='subtitle1'>Записи отсутствуют</Typography>
                </div>
              }
              {client.appointments.history.length > 0
                ?
                <div style={{ marginTop: 30, boxShadow: 'none' }}>
                    <Typography>История:</Typography>
                    <div className={classes.container} style={{ width: '100%' }}>
                      {client.appointments.history.map(el =>
                        <Appointment data={el} key={el.id}/>,
                      )}
                    </div>
                </div>
                : <div className={classes.appointments}>
                  <Typography variant='subtitle1'>Записи отсутствуют</Typography>
                </div>
              }
            </div>
          </div>
        </section>
      </div>
    </Layout>
  );
}

ClientProfile.getInitialProps = async ({ query: { id } }) => {
  const client = await get(`${server}/static/client${id}.json`)
    .then(response => response.data);
  return { client }
};

export default ClientProfile;
