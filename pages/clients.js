import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Container,
  TextField,
  Tabs,
  Tab,
  InputAdornment,
  Card,
  Avatar,
  Typography,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import Link from 'next/link';
import { get } from 'axios';
import Layout from '../components/Layout';
import { server } from '../env-config';

const useStyles = makeStyles(() => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: 10,
  },
  card: {
    display: 'flex',
    alignItems: 'center',
    padding: '7px 0px',
    margin: '10px 0px',
    borderRadius: 'unset',
  },
  avatar: {
    margin: '0px 15px 0px 10px',
    width: 50,
    height: 50,
  },
}));

function Clients({ clients }){
  const classes = useStyles();
  const [state, setState] = React.useState({ activeTab: 0, openModal: false });
  
  const toggleTabs = tab => setState({ activeTab: tab });
  
  return (
    <Layout>
      <Container className={classes.container}>
        <TextField
          placeholder='Поиск'
          InputProps={{
            startAdornment: (
              <InputAdornment position='start'>
                <SearchIcon/>
              </InputAdornment>
            ),
          }}
        />
        <Tabs
          value={state.activeTab}
          variant='fullWidth'
          onChange={(e, tab) => toggleTabs(tab)}
        >
          <Tab value={0} label='Все'/>
          <Tab value={1} label='Избранные'/>
        </Tabs>
        <section className={classes.container}>
          {clients.map(client =>
            <Link key={client.id} href={{ pathname: 'client', query: { id: client.id } }}>
              <Card className={classes.card}>
                <Avatar src={client.photo} className={classes.avatar}/>
                <Typography variant='h6'>{client.name}</Typography>
              </Card>
            </Link>,
          )}
        </section>
      </Container>
    </Layout>
  );
}

Clients.getInitialProps = async () => {
  const clients = await get(`${server}/static/clients.json`).then(result => result.data);
  return { clients };
}

export default Clients;
