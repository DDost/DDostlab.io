import React from 'react';
import App, { Container } from 'next/app';
import { PageTransition } from 'next-page-transitions';
import { ThemeProvider } from '@material-ui/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import theme from '../src/theme';

class NextApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};
    
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }
    
    return { pageProps }
  }
  
  componentDidMount() {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }
  
  render () {
    const { Component, pageProps }  = this.props;
    const ownProps = pageProps || {};
    return (
      <Container>
        <ThemeProvider theme={theme}>
          <CssBaseline/>
          <PageTransition timeout={300} classNames='page-transition'>
            <Component {...ownProps}/>
        </PageTransition>
        </ThemeProvider>
        {/* language=CSS */}
        <style jsx global>{`
          .page-transition-enter { opacity: 0 }
          .page-transition-enter-active { opacity: 1; transition: opacity 300ms }
          .page-transition-exit { opacity: 1 }
          .page-transition-exit-active { opacity: 0; transition: opacity 300ms }
        `}</style>
      </Container>
    )
  }
}

export default NextApp;
