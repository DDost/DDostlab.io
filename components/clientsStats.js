import React, { useState } from 'react';
import {
  Avatar,
  Card,
  makeStyles,
  MenuItem,
  Select,
  Typography,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMoreOutlined';
import PieChart from './charts/Pie';

const useStyles = makeStyles(() => ({
  container: {},
  header: { marginBottom: 10 },
  card: { padding: 15, margin: 10 },
  avatar: { marginRight: 10 },
  option: { display: 'flex', alignItems: 'center' },
  financeItem: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'baseline',
    width: '100%',
  },
  financeDivider: {
    borderBottom: '1px dotted #000',
    flexGrow: 1,
  },
}));

function TopClientCard({ avatar, name, appointmens, cash }) {
  const classes = useStyles();
  return (
    <Card className={classes.card}>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <Avatar src={avatar} className={classes.avatar}/>
        <Typography variant='h6'>{name}</Typography>
      </div>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <Typography
          variant='subtitle1'>Посещений: <b>{appointmens}</b></Typography>
        <Typography variant='subtitle1'>Прибыль: <b>{cash} ₽</b></Typography>
      </div>
    </Card>
  )
}

export function ActiveClients({ clientsOverall }) {
  return (
    <div style={{ marginTop: 30 }}>
      <Typography variant='subtitle1'><b>Самые активные:</b></Typography>
      <TopClientCard
        avatar={clientsOverall.top_client_w.avatar}
        name={clientsOverall.top_client_w.name}
        appointmens={clientsOverall.top_client_w.appointments}
        cash={clientsOverall.top_client_w.cash}
      />
      <TopClientCard
        avatar={clientsOverall.top_client_m.avatar}
        name={clientsOverall.top_client_m.name}
        appointmens={clientsOverall.top_client_m.appointments}
        cash={clientsOverall.top_client_m.cash}
      />
    </div>
  )
}

export function Attendance({ clients, attendance }) {
  const classes = useStyles();
  const [client, setClient] = useState(clients[0].id);
  const chartData = [
    { name: 'Посещения', value: attendance[client].total - attendance[client].cancelled },
    { name: 'Отмены', value: attendance[client].cancelled },
  ];
  
  return (
    <div style={{ marginTop: 60 }}>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <Typography variant='subtitle1'><b>Посещаемость</b></Typography>
        <Select onChange={e => setClient(e.target.value)} value={client}>
          {clients.map(el =>
            <MenuItem key={el.id} value={el.id}>
              <div className={classes.option}>
                <Avatar src={el.photo} style={{ marginRight: 10 }}/>
                <Typography variant='body1'>{el.name}</Typography>
              </div>
            </MenuItem>
          )}
        </Select>
      </div>
      <PieChart data={chartData} showTotalLegend={false}/>
    </div>
  )
}

export function IncomesAndExpenses({ data }) {
  const classes = useStyles();
  const [month, setMonth] = useState(0);
  const months = data.map(el => el.month);
  const chartData = [
    { name: 'Доходы', value: data[month].income.total },
    { name: 'Расходы', value: data[month].expense.total },
  ];
  
  return (
    <div style={{ marginTop: 60 }}>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <Typography variant='subtitle1'><b>Доходы / расходы</b></Typography>
        <Select onChange={e => setMonth(e.target.value)} value={month}>
          {months.map((el, index) =>
            <MenuItem key={el} value={index}>
              <Typography variant='body1'>{months[index]}</Typography>
            </MenuItem>,
          )}
        </Select>
      </div>
      <PieChart data={chartData} showTotalLegend={false} units='₽'/>
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
          <Typography className={classes.financeItem} variant='h6'>
            <span>Прибыль:</span>
            <span className={classes.financeDivider}/>
            <b>{data[month].income.total - data[month].expense.total} ₽</b>
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <section style={{ marginTop: 10, width: '100%' }}>
            {data[month].income.by_service.map(el =>
              <Typography className={classes.financeItem} variant='body1' key={`${el.name}_${el.value}`}>
                <span>{el.name}:</span>
                <span className={classes.financeDivider}/>
                <b>{el.value} ₽</b>
              </Typography>)
            }
          </section>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <div style={{ margin: '50px 0px' }}>
        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
            <Typography className={classes.financeItem} variant='h6'>
              <span>Расходы:</span>
              <span className={classes.financeDivider}/>
              <b>{data[month].expense.total} ₽</b>
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <section style={{ marginTop: 10, width: '100%' }}>
              {data[month].expense.by_category.map(el =>
                <Typography className={classes.financeItem} variant='body1'
                            key={`${el.name}_${el.value}`}>
                  <span>{el.name}:</span>
                  <span className={classes.financeDivider}/>
                  <b>{el.value} ₽</b>
                </Typography>)
              }
            </section>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </div>
    </div>
  );
}
