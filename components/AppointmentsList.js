import React, { useState, useEffect } from 'react';
import { get } from 'axios';
import { Container, makeStyles, Typography } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import Appointment from './Appointment';
import { server } from '../env-config';

const useAppointmentsListStyles = makeStyles(theme => ({
  container: {
    marginTop: 10,
    padding: 0,
    height: 'fit-content',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
    position: 'relative',
  },
  loading: { margin: '40px 10px 0px 10px' },
  title: {
    width: '100%',
    textAlign: 'left',
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    width: '100%',
    alignItems: 'center',
    marginBottom: 50,
  },
  add: {
    minWidth: 'initial',
    width: 35,
    height: 35,
    borderRadius: 'unset',
    color: theme.palette.primary.accent,
  },
  total: { marginRight: 10 },
}));

function AppointmentsList() {
  const classes = useAppointmentsListStyles();
  const [appointments, setAppointments] = useState(null);
  
  useEffect(() => {
    get(`${server}/static/appointments.json`)
      .then(({ data }) =>  setAppointments(data))
  }, []);
  
  return appointments ? (
    <Container className={classes.container}>
      <h2 className={classes.title}>Записи:</h2>
      <div className={classes.header}>
        <AddIcon className={classes.add}/>
        <Typography className={classes.total} varinat='h2'>
          Всего: <b color='secondary'>{appointments.length}</b>
        </Typography>
      </div>
      {appointments.map(el => <Appointment key={el.id} {...el}/>)}
    </Container>
  ) : (<Typography variant='subtitle1' className={classes.loading}>Загрузка...</Typography>);
}

export default AppointmentsList;
